import flask_sqlalchemy

db = flask_sqlalchemy.SQLAlchemy()

class DonVi(db.Model):
    __tablename__='donvi'

    madv = db.Column(db.String(20), primary_key=True)
    tendv = db.Column(db.String(30))

    #tao lien ket voi MinhChung phat hanh
    minhchung = db.relationship('MinhChung', secondary='mahoaminhchung', backref='donvi', lazy="joined")

    #tao lien ket voi NguoiDung
    nguoidung = db.relationship('NguoiDung', backref='donvi', lazy="joined")

    def as_dict(self):
      return {c.name: str(getattr(self, c.name)) for c in self.__table__.columns}


class MinhChung(db.Model):
    __tablename__='minhchung'

    mamc = db.Column(db.Integer, primary_key=True, autoincrement=True)
    tenmc = db.Column(db.String(30))
    ngaybanhanh = db.Column(db.DateTime)
    url = db.Column(db.String(100))
    sobanhanh = db.Column(db.String(20))
    mota = db.Column(db.String  (255))
    trangthai = db.Column(db.Boolean)

    #tao khoa ngoai lien ket toi LoaiMinhChung
    maloai = db.Column(db.Integer, db.ForeignKey('loaiminhchung.maloai'), nullable=False)
    madv = db.Column(db.String(20), db.ForeignKey('donvi.madv'), nullable=False)

    #liet ke nhung ai da thao tac voi minh chung dua vao log_nd_mc
    nguoidung = db.relationship('NguoiDung', secondary='log_nd_mc', backref='minhchung', lazy="joined")

    def as_dict(self):
       return {c.name: str(getattr(self, c.name)) for c in self.__table__.columns}

        
class LoaiMinhChung(db.Model):
    __tablename__='loaiminhchung'

    maloai = db.Column(db.Integer, primary_key=True, autoincrement=True)
    tenloai = db.Column(db.String(30))

    #tao lien ket voi  MinhChung
    minhchung = db.relationship('MinhChung', backref='loaiminhchung', lazy="joined")

    def as_dict(self):
      return {c.name: str(getattr(self, c.name)) for c in self.__table__.columns}


class MaHoaMinhChung(db.Model):
    __tablename__='mahoaminhchung'

    idmahoa = db.Column(db.String(50), primary_key=True)
    madv = db.Column(db.String(20), db.ForeignKey('donvi.madv'), primary_key=True)
    mamc = db.Column(db.Integer, db.ForeignKey('minhchung.mamc'), primary_key=True)
    nam = db.Column(db.Integer, primary_key=True)
    tieuchi = db.Column(db.String(20), db.ForeignKey('tieuchi.matieuchi'), nullable=False)


class NguoiDung(db.Model):
    __tablename__='nguoidung'

    email = db.Column(db.String(30), primary_key=True)
    hoten = db.Column(db.String(50))
    matkhau = db.Column(db.String(50))
    sodt = db.Column(db.String(15))
    hinhanh = db.Column(db.String(50))
    madv = db.Column(db.String(20), db.ForeignKey('donvi.madv'), nullable=False)
    loaind = db.Column(db.Integer, db.ForeignKey('loainguoidung.maloai'), nullable=False)


class LOG_ND_MC(db.Model):
    __tablename__='log_nd_mc'

    email = db.Column(db.String(30), db.ForeignKey('nguoidung.email'), primary_key=True)
    mamc = db.Column(db.Integer, db.ForeignKey('minhchung.mamc'), primary_key=True)
    thoigian = db.Column(db.DateTime, primary_key=True)
    hanhdong = db.Column(db.String(255))


class LoaiNguoiDung(db.Model):
    __tablename__='loainguoidung'

    maloai = db.Column(db.Integer, primary_key=True)
    ten = db.Column(db.String(20))

    #lien ket voi NguoiDung
    nguoidung = db.relationship('NguoiDung', backref='loainguoidung', lazy="joined")
    quyen = db.relationship('Quyen', secondary='phanquyen', backref='loainguoidung', lazy="joined")


class Quyen(db.Model):
    __tablename__='quyen'

    maquyen = db.Column(db.Integer, primary_key=True)
    ten = db.Column(db.String(20))


class PhanQuyen(db.Model):
    __tablename__='phanquyen'

    loaind = db.Column(db.Integer, db.ForeignKey('loainguoidung.maloai'), primary_key=True)
    maquyen = db.Column(db.Integer, db.ForeignKey('quyen.maquyen'), primary_key=True)
    mota = db.Column(db.String(255))
    

class BaoCao(db.Model):
  __tablename__='baocao'

  donvi = db.Column(db.String(20), db.ForeignKey('donvi.madv'), nullable=False)
  nguoidang = db.Column(db.String(30), db.ForeignKey('nguoidung.email'), nullable=False)
  nam = db.Column(db.Integer, primary_key=True)
  url = db.Column(db.String(100))


class TieuChi(db.Model):
  __tablename__='tieuchi'

  matieuchi = db.Column(db.String(20), primary_key=True)
  noidung = db.Column(db.String(255), nullable=False)
  matieuchuan = db.Column(db.String(20), db.ForeignKey('tieuchuan.matieuchuan'), nullable=False)


class TieuChuan(db.Model):
  __tablename__='tieuchuan'
  
  matieuchuan = db.Column(db.String(20), primary_key=True)
  noidung = db.Column(db.String(255), nullable=False)
  nambanhanh = db.Column(db.Integer, nullable=False)
  tieuchi = db.relationship('TieuChi', backref='tieuchuan', lazy="joined")
