from flask import Flask, jsonify, request
from models import *
from flask_cors import CORS
from flask_jwt_extended import JWTManager, jwt_required, create_access_token, create_refresh_token, get_jwt_identity
from sqlalchemy import and_, String, cast

# from werkzeug.security import check_password_hash, generate_password_hash

import json
import config
import database


def create_app():
  flask_app = Flask(__name__)
  print(config.DATABASE_CONNECTION_URI)
  flask_app.config['SQLALCHEMY_DATABASE_URI'] = config.DATABASE_CONNECTION_URI
  flask_app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
  flask_app.config['JWT_SECRET_KEY'] = config.JWT_SECRET_KEY
  flask_app.app_context().push()
  db.init_app(flask_app)
  db.create_all()
  return flask_app

app = create_app()
# app = Flask(__name__)
JWTManager(app)
CORS(app)
resources = {r"/api/*": {"origins": "*"}}
app.config["CORS_HEADERS"] = "Content-Type"
app.config['JSON_SORT_KEYS'] = False

@app.route('/')
def home():
    return jsonify({"Message":"Hello Khoa! This is your flask app with docker"})



@app.route('/v1/login', methods=['POST'])
def _login():
  email = request.json.get('email', '')
  matkhau = request.json.get('password', '')
  nd = NguoiDung.query.filter_by(email=email).first()

  if nd:
    # is_pass_correct = check_password_hash(user.password, password)
    is_pass_correct = nd.matkhau == matkhau

    if is_pass_correct:
      refresh = create_refresh_token(identity={"email": nd.email, "hoten": nd.hoten, "role": nd.loaind})
      access = create_access_token(identity=nd.email)

      return jsonify({
        'user': {
                    'refresh': refresh,
                    'access': access
                }}), 200
  return jsonify({'error': 'Wrong credentials'}), 401


@app.route('/v1/me', methods=['GET'])
@jwt_required()
def me():
    email = get_jwt_identity()
    nd = NguoiDung.query.filter_by(email=email).first()

    return jsonify({
        'hoten': nd.hoten,
        'email': nd.email,
        'role': nd.loaind
    }), 200


@app.route('/proof/search', methods=['POST'])
@jwt_required()
def searchProof():
  req = request.get_json()
  exp1, exp2 = [], []
  
  if req.get('tendv'):
    exp1 = [DonVi.tendv.ilike("%{}%".format(req.get('tendv')))] 
    del req['tendv']
  if req.get('tenloai'):
    exp2 = [LoaiMinhChung.tenloai.ilike("%{}%".format(req.get('tenloai')))]
    del req['tenloai']

  for col, val in req.items():
    rs = db.session.query(MinhChung, DonVi, LoaiMinhChung).join(DonVi).filter(*exp1).join(LoaiMinhChung).filter(*exp2).\
                    filter(and_(*[cast(getattr(MinhChung, col), String).ilike("%{}%".format(val))\
                                                               for col, val in req.items()])).all()
  rs = [it[0].as_dict() | it[1].as_dict() | it[2].as_dict() for it in rs]
  return jsonify(rs)


@app.route('/proof/add', methods=['POST'])
@jwt_required()
def addProof():
  kwargs = request.get_json() or {}
  instance = MinhChung(**kwargs)
  try:
      db.session.add(instance)
      db.session.commit()
  except Exception:
      db.session.rollback()
      instance = MinhChung.query.filter_by(**kwargs).one()
      return instance.as_dict(), False
  else:
      return instance.as_dict(), True


@app.route('/proof/update', methods=['POST'])
@jwt_required()
def updateProof():
  kwargs = request.get_json() or {}
  try:
      instance = MinhChung.query.filter_by(mamc=kwargs.get('mamc', ''))
      instance.update(kwargs)
      db.session.commit()

  except Exception:
      db.session.rollback()
      return instance, False
  else:
      return instance.first().as_dict(), True


@app.route('/proof/delete', methods=['POST'])
@jwt_required()
def deleteProof():
  kwargs = request.get_json() or {}
  try:
      instance = MinhChung.query.filter_by(mamc=kwargs.get('mamc', ''))
      instance.delete()
      db.session.commit()

  except Exception:
      db.session.rollback()
      return instance, False
  else:
      return instance.first().as_dict(), True


@app.route('/proof/type', methods=['GET'])
@jwt_required()
def getTypes():
  rs = [it.as_dict() for it in LoaiMinhChung.query.all()];
  return jsonify(rs)


@app.route('/department/all', methods=['GET'])
@jwt_required()
def getDepartments():
  rs = [it.as_dict() for it in DonVi.query.all()];
  return jsonify(rs)

@app.route('/department/search', methods=['POST'])
@jwt_required()
def searchDepart():
  req = request.get_json()
  for col, val in req.items():
    rs = DonVi.query.filter(or_(*[getattr(DonVi, col).ilike("%{}%".format(val)) for col, val in req.items()])).all()
  rs = [it.as_dict() for it in rs]
  return jsonify(rs)


@app.route('/department/add', methods=['POST'])
@jwt_required()
def addDepart():
  kwargs = request.get_json() or {}
  instance = DonVi(**kwargs)
  try:
      db.session.add(instance)
      db.session.commit()
  except Exception:
      db.session.rollback()
      instance = DonVi.query.filter_by(**kwargs).one()
      return instance.as_dict(), False
  else:
      return instance.as_dict(), True


@app.route('/department/update', methods=['POST'])
@jwt_required()
def updateDepart():
  kwargs = request.get_json() or {}
  try:
      instance = DonVi.query.filter_by(madv=kwargs.get('madv', ''))
      instance.update(kwargs)
      db.session.commit()

  except Exception:
      db.session.rollback()
      return instance, False
  else:
      return instance.first().as_dict(), True


@app.route('/department/delete', methods=['POST'])
@jwt_required()
def deleteDepart():
  kwargs = request.get_json() or {}
  try:
      instance = DonVi.query.filter_by(madv=kwargs.get('madv', ''))
      instance.delete()
      db.session.commit()

  except Exception:
      db.session.rollback()
      return instance, False
  else:
      return instance.first().as_dict(), True


#####################################################################################################################
@app.route('/login', methods=['POST'])
def login():
    args = request.get_json()
    usr = args.get('username')
    passw = args.get('password')

    if usr=="admin@sgu.edu.vn" and passw == "123":
      return jsonify({"email":"admin@gmail.com", "role":1})
    elif usr=='kdv@sgu.edu.vn' and passw == '123':
      return jsonify({"email":"kdv@gmail.com", "role":2})
    elif usr=='kt@sgu.edu.vn' and passw == '123':
      return jsonify({"email":"kt@gmail.com", "role":3})
    elif usr=='vbc@sgu.edu.vn' and passw == '123':
      return jsonify({"email":"vbc@gmail.com", "role":4})
    else:
      return jsonify({"email":"test@gmail.com", "role":5})

if __name__ == "__main__":
    app.run(debug = True, host='0.0.0.0', port=7007)

