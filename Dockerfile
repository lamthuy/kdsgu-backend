FROM python:3.9.0-alpine
WORKDIR /code

RUN apk --update --upgrade add --no-cache  gcc musl-dev jpeg-dev zlib-dev libffi-dev cairo-dev pango-dev gdk-pixbuf-dev

RUN python -m pip install --upgrade pip
COPY requirements.txt requirements.txt

RUN apk add --no-cache --virtual .build-deps gcc gdb libc-dev libxslt-dev && \
    apk add --no-cache libxslt && \
    apk add --no-cache postgresql-dev && \
    pip install --no-cache-dir lxml>=3.5.0 && \
    apk del .build-deps

RUN apk add build-base
RUN pip install -r requirements.txt
EXPOSE 7007
COPY . .
CMD [ "python", "webapp/app.py" ]
